/**
 Copyright 2020 Miriam Jiménez
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package interfaz;

import dominio.*;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;
import java.util.InputMismatchException;

/**
 * Esta es la clase Interfaz que configurará el menú a través del cual se podrán consultar las distintas opciones a ejecutar
 * 
 * @author Miriam Jiménez
 * @version primera entrega
 */
public class Interfaz{
    /**
     * Método que se encarga de llamar al menú y gestionar la ejecución de las distintas opciones selccionadas por el usuario
     * 
     */
    public static void iniciaAplicacion(){
        Concesionario concesionario = incializarConcesionario("catalogoConcesionario.txt");
        
        Scanner teclado = new Scanner(System.in);
        int opcion;
        do {
            opcion = menu(teclado);
            switch (opcion) {
            case 0: //Sale del programa
                inicializarFichero("catalogoConcesionario.txt",concesionario);
                break;
            case 1:{  // Consulta un coche
                int identificador;
                while(true){
                    try{
                        System.out.print("Introduce el identificador del coche: ");
                        identificador = teclado.nextInt();
                        break;
                    } catch(InputMismatchException ex){
                        System.out.println("ERROR: Solo se puede introducir un número entero.");
                        teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
                    }
                }
                String coche = concesionario.consultarCoche(identificador);
                if(coche == null)
                    System.out.println("Coche con identificador " + identificador + " no encontrado.");
                else
                    System.out.println("Coche añadido: " + coche);
                break;}
            case 2:{  // Consulta el catologo.
                System.out.println(concesionario.toString());
                break;}
            case 3:{  // Dar de alta un coche
                System.out.print("Introduce la marca del coche: ");
                String marca = teclado.nextLine();
                System.out.print("\nIntroduce el modelo del coche: ");
                String modelo = teclado.nextLine();
                int numeroDePuertas;
                while(true){
                    try{
                        System.out.print("\nIntroduce el número de puertas del coche: ");
                        numeroDePuertas = teclado.nextInt();
                        break;
                    } catch(InputMismatchException ex){
                        System.out.println("ERROR: Solo se puede introducir un número entero.");
                        teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
                    }
                }
                teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
                String automatico = "";
                do{
                    System.out.print("\n¿Es automatico? (S/N): ");
                    automatico = teclado.nextLine();
                } while(!automatico.equals("S") && !automatico.equals("N"));
                boolean marcha = automatico.equals("S") ? true : false;
                String coche = concesionario.annadirCoche(marca, modelo, marcha, numeroDePuertas);
                System.out.println("\nAñadido el coche: " + coche);
                break;}
            case 4:{ //Eliminar un coche del catalogo
                int identificador;
                while(true){
                    try{
                        System.out.print("Introduce el identificador del coche: ");
                        identificador = teclado.nextInt();
                        break;
                    } catch(InputMismatchException ex){
                        System.out.println("ERROR: Solo se puede introducir un número entero.");
                        teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
                    }
                }
                String coche = concesionario.eliminarCoche(identificador);
                if(coche == null)
                    System.out.println("Coche con identificador " + identificador + " no encontrado.");
                else
                    System.out.println("Coche eliminado: " + coche);
                break;}
            case 5:{ //Modificar un coche del catalogo
                int identificador;
                while(true){
                    try{
                        System.out.print("Introduce el identificador del coche: ");
                        identificador = teclado.nextInt();
                        break;
                    } catch(InputMismatchException ex){
                        System.out.println("ERROR: Solo se puede introducir un número entero.");
                        teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
                    }
                }
                teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
                System.out.print("\nIntroduce la marca del coche: ");
                String marca = teclado.nextLine();
                System.out.print("\nIntroduce el modelo del coche: ");
                String modelo = teclado.nextLine();
                int numeroDePuertas;
                while(true){
                    try{
                        System.out.print("\nIntroduce el número de puertas del coche: ");
                        numeroDePuertas = teclado.nextInt();
                        break;
                    } catch(InputMismatchException ex){
                        System.out.println("ERROR: Solo se puede introducir un número entero.");
                        teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
                    }
                }
                teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
                String automatico = "";
                do{
                    System.out.print("\n¿Es automatico? (S/N): ");
                    automatico = teclado.nextLine();
                } while(!automatico.equals("S") && !automatico.equals("N"));
                boolean marcha = automatico.equals("S") ? true : false;
                String coche = concesionario.modificarCoche(identificador, marca, modelo, marcha, numeroDePuertas);
                if(coche == null)
                    System.out.println("Coche con identificador " + identificador + " no encontrado.");
                else
                    System.out.println("Coche modificado: " + coche);
                break;}
            case 6:{ //Generar hoja de calculo
                generarCSV("catalogoConcesionario.csv", concesionario);
                System.out.println("Archivo CSV generado.");
                break;}
            } // fin switch

        } while (opcion != 0);
    }

    /**
     * Método que dibuja el menú de texto y recoge los inputs del usuario 
     * 
     * @param teclado   objeto de la clase scanner para recoger los inputs del teclado 
     * @return          devuelve la opción seleccionada del menú 
     */
    public static int menu(Scanner teclado) {
        int opcion;
        System.out.println("\n\n");
        System.out.println("=====================================================");
        System.out.println("============            MENU        =================");
        System.out.println("=====================================================");
        System.out.println("0. Salir.");
        System.out.println("1. Consultar un coche.");
        System.out.println("2. Consultar el catalogo.");
        System.out.println("3. Alta de un coche.");
        System.out.println("4. Baja de un coche.");
        System.out.println("5. Modificar un coche.");
        System.out.println("6. Exportar a hoja de calculo.");
        do {
            System.out.print("\nElige una opcion (0..6): ");
            opcion = teclado.nextInt();
        } while ((opcion < 0) || (opcion > 6));
        teclado.nextLine(); // Elimina retorno de carro del buffer de entrada
        return opcion;
    }
    
    /**
     * Método que escribe en un fichero con los datos que se le pasan 
     * 
     * @param nombreFichero   cadena con el nombre del fichero
     * @param texto           cadena con la infromación a escribir en el fichero
     */
    private static void inicializarFichero(String nombreFichero, Concesionario concesionario){
        try{
            FileWriter fw = new FileWriter(nombreFichero);
            fw.write(concesionario.getIdentificador() + "\n");
            fw.write(concesionario.toString());
            fw.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    
    /**
     * Método que lee los datos de un fichero o los crea en caso de no existir
     * 
     * @param nombreFichero  cadena con el nombre del fichero    
     * @return          devuelve un objeto de la clase Concesionario  
     */
    private static Concesionario incializarConcesionario(String nombreFichero){
        Concesionario concesionario = new Concesionario();
        try{
            File file = new File(nombreFichero);
            Scanner sc = new Scanner(file);
            int identificador = Integer.parseInt(sc.nextLine());
            concesionario = new Concesionario(identificador);
            while(sc.hasNext()){
                String linea = sc.nextLine();
                String[] parts = linea.split("#");
                boolean esAutomatico = parts[3].equals("Automática") ? true : false;
                Coche coche = new Coche(Integer.parseInt(parts[0]), parts[1], parts[2], esAutomatico, Integer.parseInt(parts[4]));
                concesionario.annadirCoche(coche);
            }
            sc.close();
        } catch (FileNotFoundException e){
            inicializarFichero(nombreFichero, concesionario);
        } catch (Exception e){
            System.out.println(e);
        }
        return concesionario;
    }
    
    /**
     * Método que escribe en un fichero CSV los datos de un concesioanrio
     * 
     * @param nombreCSV       cadena con el nombre del fichero CSV
     * @param concesionario   Concesionario a pasar a formato CSV 
     */
     private static void generarCSV(String nombreCSV, Concesionario concesionario){
        try{
            FileWriter fw = new FileWriter(nombreCSV);
            fw.write(concesionario.toCSV());
            fw.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }
}

