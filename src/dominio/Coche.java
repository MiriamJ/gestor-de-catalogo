/**
 Copyright 2020 Miriam Jiménez
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
	
	http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License
*/

package dominio;
/**
 * En la clase Coche se añaden los atributos del objeto coche  
 * 
 * @author (Miriam Jiménez) 
 * @version (primera entrega)
 */
public class Coche
{ 
    private String marca;
    private String modelo;
    private boolean marchaAutomatica;
    private int numeroDePuertas;
    private int identificador;

    /**
     * Constructor para los objetos de la clase Coche
     * 
     * @param identificador  identificador del Coche 
     * @param  marca cadena que identifica la marca del Coche
     * @param  modelo cadena que identifica el modelo del Coche
     * @param  marchaAutomatica booleano que identifica si un Coche tiene marcha automatica
     * @param numeroDePuertas  el numero de puertas que tiene el coche
     */
    public Coche(int identificador, String marca,String modelo,boolean marchaAutomatica, int numeroDePuertas)
    {
        this.identificador = identificador;
        this.marca = marca;
        this.modelo = modelo;
        this.marchaAutomatica = marchaAutomatica;
		this.numeroDePuertas = numeroDePuertas;
    }
    
    /**
     * Método que muestra el identificador del coche 
     * 
     * @return     devuelve el identificador del coche 
     */
    public int getIdentificador()
    {
        return this.identificador;
    }

    /**
     * Método que muestra la marca del coche 
     * 
     * @return         devuelve la marca del coche
     */
    public String getMarca()
    {
        return this.marca;
    }
    
    /**
     * Método que muestra el modelo del coche 
     * 
     * @return         devuelve el modelo del coche
     */
    public String getModelo()
    {
        return this.modelo;
    }
    
    /**
     * Método de tipo booleano que muestra la marcha que tiene el coche 
     * 
     * @return         devuelve la marcha del coche
     */
    public boolean getMarchaAutomatica()
    {
        return this.marchaAutomatica;
    }
    
	/**
     * Método de tipo entero que indica el número de puertas que tiene del coche 
     * 
     * @return     devuelve el número de puertas del coche 
     */
    public int getNumeroDePuertas()
    {
        return this.numeroDePuertas;
    }
	
    /**
     * Método que muestra la información completa del coche  
     * 
     * @return     devuelve una cadena con la información del Coche
     */
    public String toString()
    {
        String marcha;
        if (this.marchaAutomatica==true){
           marcha="Automática"; 
        }
        else{
            marcha="Manual";
        }
        return this.identificador + "#" + this.marca + "#" + this.modelo + "#" + marcha + "#" + numeroDePuertas + "\n" ;
    }
    
    /**
     * Método que devuelve el Coche en formato CSV   
     * 
     * @return     devuelve una cadena con la información del Coche en formato CSV
     */
    public String toCSV()
    {
        String marcha;
        if (this.marchaAutomatica==true){
           marcha="Automática"; 
        }
        else{
            marcha="Manual";
        }
        return this.identificador + "," + this.marca + "," + this.modelo + "," + marcha + "," + numeroDePuertas + "\n";
    }
}
