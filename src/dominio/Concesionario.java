/**
 Copyright 2020 Miriam Jiménez
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 	
	http://www.apache.org/licenses/LICENSE-2.0
 	
	Unless required by applicable law or agreed to in writing, software
 	distributed under the License is distributed on an "AS IS" BASIS,
 	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 	See the License for the specific language governing permissions and
 	limitations under the License.
*/

package dominio;
import java.util.ArrayList;

/**
 * Se encarga de la gestión de los coches de un concesionario
 * 
 * @author Miriam Jiménez 
 * @version primera entrega
 */
public class Concesionario
{
    private ArrayList<Coche> catalogoCoches; //
    private int identificador; // El siguiente identificador interno que se asignara en el concesionario

    /**
     * Constructor para los objetos de la clase Concesionario 
     * 
     */
    public Concesionario()
    {
        catalogoCoches = new ArrayList<>();
    }
	
	/**
     * Constructor para los objetos de la clase Concesionario, con un identificador
     * 
     */
    public Concesionario(int idenfiticador)
    {
        catalogoCoches = new ArrayList<>();
		this.identificador = idenfiticador;
    }
	
     /**
     * Método para buscar un Coche en el catálogo de coches
     * 
     * @param  identificador      el identificador del coche para buscarlo
     * @return                    el Coche buscado 
     */
    private Coche buscarCoche(int identificador)
    {
        Coche coche = null;
        for(Coche c : catalogoCoches)
            if(c.getIdentificador() == identificador)
                coche = c;
        return coche;
    }


    /**
     * Metodo para añadir un coche al catalogo del concesionario
     * 
     * @param  marca cadena que identifica la marca del coche
     * @param  modelo cadena que identifica el modelo del coche
     * @param  esAutomatico booleano que identifica si un coche tiene marcha automatica
	 * @param  numeroDePuertas entero que indica el número de puertas que tiene el coche
     * @return     devuelve una cadena con informacion del coche
     */
    public String annadirCoche(String marca, String modelo, boolean esAutomatico, int numeroDePuertas)
    {
        Coche coche = new Coche(identificador++, marca, modelo, esAutomatico, numeroDePuertas);
        catalogoCoches.add(coche);
        return coche.toString();
    }
    
    /**
     * Método que añade un coche al catalogo del concesionario
     * 
     * @param  coche   Coche a añadir al catálogo
     */
    public void annadirCoche(Coche coche)
    {
        catalogoCoches.add(coche);
    }

    /**
     * Método para consultar un Coche guardado en el catálogo
     * 
     * @param identificador     identificador del coche a consultar 
     * @return                  devuelve una cadena con la información del Coche consultado, o null en caso de que no se encuentre el Coche
     */
    public String consultarCoche(int identificador)
    {
        Coche coche = buscarCoche(identificador);
        if(coche != null)
            return coche.toString();
        return null;
    }

    /**
     * Método para eliminar un coche del catálogo 
     * 
     * @param identificador    idenfiticador del Coche a eliminar
     * @return                 devuelve una cadena con la información del Coche eliminado, o null en caso de que no se encuentre el Coche      
     */
    public String eliminarCoche(int identificador)
    {
        Coche coche = buscarCoche(identificador);
        
        if(coche!=null){
            catalogoCoches.remove(coche);
            return coche.toString();
        }

        return null;
    }

    /**
     * Método que modifica las características de un coche
     * 
     * @param identificador  identificador del Coche a modificar
     * @param  marca cadena que identifica la marca del Coche
     * @param  modelo cadena que identifica el modelo del Coche
     * @param  esAutomatico booleano que identifica si un Coche tiene marcha automatica
	 * @param  numeroDePuertas  entero que indica el número de puertas que tiene un coche
     * @return     devuelve una cadena con informacion del Coche
     */
    public String modificarCoche(int identificador, String marca, String modelo, boolean esAutomatico, int numeroDePuertas)
    {
        Coche coche = buscarCoche(identificador);
        if(coche != null){
            catalogoCoches.remove(coche);
            coche = new Coche(identificador, marca, modelo, esAutomatico, numeroDePuertas);
            catalogoCoches.add(coche);
            return coche.toString();
        }
        return null;
    }
	
	/**
     * Método que devuelve un entero con el último idenfiticador del concesionario
     * 
     * @return     devuelve un entero con el idenfiticador
     */
    public int getIdentificador(){
		
        return this.identificador;
    }

    /**
     * Método que devuelve el catálogo de coches del Concesionario
     * 
     * @return     devuelve una cadena con la información del catálogo de coches  
     */
    public String toString(){
        StringBuilder cadena = new StringBuilder();
        for(Coche coche : catalogoCoches)
            cadena.append(coche.toString());
        return cadena.toString();
    }
    
    /**
     * Método que devuelve el catálogo de coches del Concesionario en formato CSV
     * 
     * @return     devuelve una cadena con la información del catálogo de coches en formato CSV 
     */
    public String toCSV(){
        StringBuilder datos = new StringBuilder();
        for (Coche coche: catalogoCoches){
            datos.append(coche.toCSV());
        }
        return datos.toString();
    }
}
