# Gestor de un concesionario
## Introducción

------------------------------------------------------------------------
El proyecto es de gestión de un catálogo de coches de un concesionario. 
Permite dar de alta, modificar y dar de baja los coches del catálogo.
------------------------------------------------------------------------

## Cómo iniciar el proyecto

El proyecto se compila usando make a través del archivo makefile: 

	$ make compilar

El proyecto se ejecuta usando:

	$ make ejecutar

Se crea la documentación del proyecto usando:

	$ make javadoc

Se crea un archivo jar usando:

	$ make jar 

** VERSION or DATE: ** Entrega final

** AUTHORS: ** Miriam Jiménez

## Licencia

Copyright 2020 Miriam Jiménez

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
	
  http://www.apache.org/licenses/LICENSE-2.0
	
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License



